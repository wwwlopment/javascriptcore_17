function sendByPOSTMethod() {
  var arr = getData();
  if (!arr) {
    return false;
  }

  ServerRequest(arr).then(
    function(response) {
      alert(response);
    },
    function(error) {
      alert(error);
    });
}


function ServerRequest(arr) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/user-data');
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.send(JSON.stringify(arr));
    xhr.onload = function() {
      if (this.status === 200) {
        resolve(this.response);
      } else {
        reject(this.statusText);
      }
    }
  });
}

function getData() {
  var data = document.getElementsByClassName('input3');
  var arr = {};
  for (var i = 0; i < data.length; i++) {
    arr[data[i].getAttribute('name')] = data[i].value;
  }
  return arr;
}
